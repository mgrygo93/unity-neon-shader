﻿Shader "xyc1993/Neon"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_colorIntensity("Color intensity", Range(1, 3)) = 1
		_colorTransitionPower("Color transition power", Range(0, 1)) = 0.5
		_fullTransitionDuration("Full transition duration [seconds]", Range(3, 30)) = 12
	}

	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert_img //standard empty vertex shader
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			#define M_PI 3.1415926535897932384626433832795
			
			sampler2D _MainTex;
			uniform float _colorIntensity;
			uniform float _colorTransitionPower;
			uniform float _fullTransitionDuration;

			fixed4 ChangeColorWithTime(fixed4 color, float colorTransitionPower, float fullTransitionDuration)
			{
				//calculate third of a transition period
				float partDuration = fullTransitionDuration / 3.0f;

				//calculate amplitude for color transition
				float r_amplitude = (color.r > 0.5f) ? (1.0f - color.r) : color.r;
				float g_amplitude = (color.g > 0.5f) ? (1.0f - color.g) : color.g;
				float b_amplitude = (color.b > 0.5f) ? (1.0f - color.b) : color.b;

				//scale amplitude
				r_amplitude = colorTransitionPower * r_amplitude;
				g_amplitude = colorTransitionPower * g_amplitude;
				b_amplitude = colorTransitionPower * b_amplitude;

				//apply transition
				if(fmod(_Time.y, fullTransitionDuration) < partDuration) color.r = color.r * (1.0f + r_amplitude * sin(_Time.y * 2.0f * M_PI / partDuration));
				else if (fmod(_Time.y, fullTransitionDuration) < 2.0f * partDuration) color.g = color.g * (1.0f + g_amplitude * sin(_Time.y * 2.0f * M_PI / partDuration));
				else color.b = color.b * (1.0f + b_amplitude * sin(_Time.y * 2.0f * M_PI / partDuration));

				return color;
			}

			fixed4 RaiseColorIntensity(fixed4 color, float intensity) 
			{
				//if some value is equal to zero or really small, assign some other small but good for calculations value
				//this eliminates issue with dividing by zero
				if (color.r < 0.0001f) color.r = 0.001f;
				if (color.g < 0.0001f) color.g = 0.001f;
				if (color.b < 0.0001f) color.b = 0.001f;

				//calculate color amplification coefficient
				float r_amp = intensity * color.r / (color.g + color.b);
				float g_amp = intensity * color.g / (color.r + color.b);
				float b_amp = intensity * color.b / (color.r + color.g);

				//apply color amplification
				color.r = r_amp * color.r;
				color.g = g_amp * color.g;
				color.b = b_amp * color.b;

				return color;
			}

			fixed4 frag (v2f_img i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				
				col = ChangeColorWithTime(col, _colorTransitionPower, _fullTransitionDuration);
				col = RaiseColorIntensity(col, _colorIntensity);

				return col;
			}
			ENDCG
		}
	}
}
