﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControl : MonoBehaviour
{
    public ChangingNeonEffect CNE;

    public Slider neonTransitionDurationSlider;
    public Slider neonTransitionSlider;
    public Slider neonIntensitySlider;
    public Toggle effectToggle;
    public Button quitButton;

    private void Awake()
    {
        //set listeners
        neonTransitionDurationSlider.onValueChanged.AddListener(OnTransitionDurationChange);
        neonTransitionSlider.onValueChanged.AddListener(OnTransitionPowerChange);
        neonIntensitySlider.onValueChanged.AddListener(OnIntensityChange);
        effectToggle.onValueChanged.AddListener(OnEffectToggle);
        quitButton.onClick.AddListener(Quit);

        //set default values
        OnTransitionDurationChange(neonTransitionDurationSlider.value);
        OnTransitionPowerChange(neonTransitionSlider.value);
        OnIntensityChange(neonIntensitySlider.value);
        effectToggle.isOn = CNE.effectOn;
    }

    private void OnTransitionDurationChange(float value)
    {
        CNE.fullTransitionDuration = value;
        neonTransitionDurationSlider.transform.Find("Label").GetComponent<Text>().text = ("Transition duration: " + value);
    }

    private void OnTransitionPowerChange(float value)
    {
        CNE.colorTransitionPower = value;
        neonTransitionSlider.transform.Find("Label").GetComponent<Text>().text = ("Transition power: " + value);
    }

    private void OnIntensityChange(float value)
    {
        CNE.colorIntensity = value;
        neonIntensitySlider.transform.Find("Label").GetComponent<Text>().text = ("Color intensity: " + value);

        //Set UI colors
        neonIntensitySlider.transform.Find("Label").GetComponent<Text>().color = GetFontColor();
        neonTransitionSlider.transform.Find("Label").GetComponent<Text>().color = GetFontColor();
        neonTransitionDurationSlider.transform.Find("Label").GetComponent<Text>().color = GetFontColor();
        effectToggle.transform.Find("Label").GetComponent<Text>().color = GetFontColor();
    }

    private void OnEffectToggle(bool value)
    {
        CNE.effectOn = value;
    }

    private void Quit()
    {
        Application.Quit();
    }

    private Color GetFontColor()
    {
        return Color.Lerp(Color.white, Color.black, (neonIntensitySlider.value - neonIntensitySlider.minValue) / (neonIntensitySlider.maxValue - neonIntensitySlider.minValue));
    }
}
