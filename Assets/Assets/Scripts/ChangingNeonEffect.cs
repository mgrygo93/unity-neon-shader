﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ChangingNeonEffect : MonoBehaviour
{
    [Range(1.0f, 3.0f)]
    public float colorIntensity = 1.0f;
    [Range(0.0f, 1.0f)]
    public float colorTransitionPower = 0.5f;
    [Range(3.0f, 30.0f)]
    public float fullTransitionDuration = 12.0f;

    public bool effectOn = true;

    private Material material;
    
    private void Awake()
    {
        material = new Material(Shader.Find("xyc1993/Neon"));
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (material == null || !effectOn) Graphics.Blit(source, destination);
        else
        {
            material.SetFloat("_fullTransitionDuration", fullTransitionDuration);
            material.SetFloat("_colorTransitionPower", colorTransitionPower);
            material.SetFloat("_colorIntensity", colorIntensity);
            Graphics.Blit(source, null, material);
        }
    }
}